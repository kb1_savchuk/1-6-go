package main

import (
	"fmt"
	"reflect"
)

type Student struct {
	FirstName string
	LastName  string
	Age       int
	Course    int
	Group     string
}

func main() {
	student := Student{
		FirstName: "Богдана",
		LastName:  "Сак",
		Age:       20,
		Course:    3,
		Group:     "KБ-1",
	}

	v := reflect.ValueOf(student)

	for i := 0; i < v.NumField(); i++ {
		fmt.Println(v.Field(i))
	}
}
