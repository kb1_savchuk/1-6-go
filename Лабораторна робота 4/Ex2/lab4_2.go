package main

import (
	"github.com/andlabs/ui"
	"strconv"
)

func initGUI() {
	window := ui.NewWindow("Lab4 2", 500, 600, false)
	label1 := ui.NewLabel("Кількість днів")
	daysInput := ui.NewEntry()
	label2 := ui.NewLabel("Країна")
	combo1 := ui.NewCombobox()
	combo1.Append("Болгарія")
	combo1.Append("Німеччина")
	combo1.Append("Польща")
	label3 := ui.NewLabel("Період")
	combo2 := ui.NewCombobox()
	combo2.Append("Літо")
	combo2.Append("Зима")
	checkbox1 := ui.NewCheckbox("Гід")
	checkbox2 := ui.NewCheckbox("Номер люкс")
	button := ui.NewButton("Розрахувати")
	labelResult := ui.NewLabel("")

	box := ui.NewVerticalBox()
	box.Append(label1, false)
	box.Append(daysInput, false)
	box.Append(label2, false)
	box.Append(combo1, false)
	box.Append(label3, false)
	box.Append(combo2, false)
	box.Append(checkbox, false)
	box.Append(button, false)
	box.Append(labelResult, false)
	window.SetChild(box)

	button.OnClicked(func(*ui.Button) {
		daysValue, _ := strconv.ParseFloat(daysInput.Text(), 32)
		result := daysValue

		if combo1.Selected() == 1 && combo2.Selected() == 1 {
			result *= 100
		}
		if combo1.Selected() == 1 && combo2.Selected() == 2 {
			result *= 150
		}
		if combo1.Selected() == 2 && combo2.Selected() == 1 {
			result *= 160
		}
		if combo1.Selected() == 2 && combo2.Selected() == 2 {
			result *= 200
		}
		if combo1.Selected() == 3 && combo2.Selected() == 1 {
			result *= 120
		}
		if combo1.Selected() == 3 && combo2.Selected() == 2 {
			result *= 180
		}

		if checkbox1.Checked() {
			result += daysValue * 50
		}

		if checkbox2.Checked() {
			result *= 1.2
		}

		labelResult.SetText("Результат = " + strconv.FormatFloat(result, 'g', 1, 64))
	})

	window.OnClosing(func(*ui.Window) bool {
		ui.Quit()
		return true
	})
	window.Show()
}

func main() {
	err := ui.Main(initGUI)
	if err != nil {
		panic(err)
	}
}
