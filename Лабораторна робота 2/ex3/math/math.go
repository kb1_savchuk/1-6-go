package math

func searchMin(x1 float64, x2 float64, x3 float64) float64 {
	if x1 <= x2 && x1 <= x3 {
		return x1
	} else if x2 <= x1 && x2 <= x3 {
		return x2
	} else {
		return x3
	}
}

func avarageValue(x1 float64, x2 float64, x3 float64) float64 {
	return (x1 + x2 + x3) / 3
}
