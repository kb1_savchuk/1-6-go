<!DOCTYPE HTML>
<html>
   <head>
      <title>{{ .Title }}</title>
      <style>
         .error{
         color:#FF0000;
         }
      </style>
   </head>
   <body>
      <h1>{{ .Heading }}</h1>
      <form action="/" method="POST">
         <label for="firstName">You name</label><br />
         <input type="text" name="firstName" required><br />
         <label for="lastName">You last name</label><br />
         <input type="text" name="lastName" required><br />
         <label for="age">You age</label><br />
         <input type="text" name="age" required><br />
         <label for="phone">You phone</label><br />
         <input type="text" name="phone" required><br />
         <input type="submit" name="button" value="CLICK">
      </form>
      {{ if .IsPost }}
      <div>Hello, {{ .FirstName }}</div>
      <div>Your second name is, {{ .LastName }}</div>
      <div>Your age is, {{ .Age }}</div>
      <div>Your phone number is, {{ .Phone }}</div>
      {{ end }}
   </body>
</html>