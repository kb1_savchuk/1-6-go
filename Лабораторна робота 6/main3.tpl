<!DOCTYPE HTML>
<html>
   <head>
      <title>{{ .Title }}</title>
      <style>
         .error{
         color:#FF0000;
         }
      </style>
   </head>
   <body>
      <h1>{{ .Heading }}</h1>
      <p>Это описание задания</p>
      <form action="/" method="POST">
         <input type="submit" value="Go to page 1">
      </form>
      <div>Hello, {{ .FirstName }}</div>
      <div>Your second name is, {{ .LastName }}</div>
      {{ if .IsPost }}
      {{ end }}
   </body>
</html>