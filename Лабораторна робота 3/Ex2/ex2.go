package main

import (
	"fmt"
	"math"
	"math/rand"
	random "ex2/random"
)

func main() {
	m := 2147483647.0
	a := 2147483629.0
	c := 2147483587.0
	n := 2000
	maxRange := 250
	arr := make([]float64, n)
	random.random(arr, m, a, c, n, maxRange)
	fmt.Println(arr)
}

